package debug.question;

public class Question4 {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		String str1 = "As Leading Harmonaizer";
		String str2 = "As Leading ";
		String str3 = "Harmonaizer";
		String str4 = str2 + str3;
//		String str5 = str1;

		boolean result = stringComparison(str1, str4);
		System.out.println(result);
	}

	public static boolean stringComparison(String a, String b) {
//		return a == b;
		return a.equals(b);
	}

}