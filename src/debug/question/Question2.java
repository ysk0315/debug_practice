package debug.question;

public class Question2 {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		String str1 = "Harmonize!";
		String str2 = "Harmo";
		String str3 = str2 + "nize!";

		boolean result = stringComparison(str1, str3);
		System.out.println(result);

//		boolean result2 = debug.question.Question4.stringComparison(str1, str3);
//		System.out.println(result2);
	}

	public static boolean stringComparison(String str1, String str2) {
//		return str1 == str2;
		return str1.equals(str2);
	}

}
