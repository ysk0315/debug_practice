package debug.question;

public class Question6 {

	public static void main(String[] args) {
		// TODO 自動生成されたメソッド・スタブ

		int numFizz = 2;
		int numBuzz = 3;
		String strFizz = "Fizz";
		String strBuzz = "Buzz";

		for(int i = 1; i <= 20; i++) {
			if(i % (numFizz * numBuzz) == 0) {
				System.out.println(strFizz + strBuzz);
			} else if(i % numFizz == 0) {
				System.out.println(strFizz);
			} else if(i % numBuzz == 0) {
				System.out.println(strBuzz);
			} else {
				System.out.println(i);
			}
		}
	}

}
